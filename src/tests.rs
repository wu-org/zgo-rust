use crate::my::*;
#[test]
fn new_and_go() {
	World::new(9, 9)
		.go('0', 1, 0)
		.go('0', -1, 0)
		.go('0', 0, 1)
		.go('0', 0, -1);
	assert_eq!(
		World::new(9, 9)
		.go('0', 1, 0)
		.go('0', -1, 0)
		.go('0', 0, 1)
		.go('0', 0, -1)
		,
		World::new(9, 9)
		.go('0', 1, 0)
		.go('0', 0, 1)
		.go('0', -1, 0)
		.go('0', 0, -1)
	);
}
#[test]
fn from_str() {
	assert_eq!(
		World::from (
"\
_________
_________
_________
___2_1___
____0____
___3_4___
_________
_________
_________"
		).board
		,
		*World::new(9, 9)
			.go('0',  0,  0)
			.go('1',  1,  1)
			.go('2', -1,  1)
			.go('3', -1, -1)
			.go('4',  1, -1).board
	);
}

//#[test]
fn count_chi_test0(){
	assert_eq!(World::from("0").chi(RealPos{row:0,col:0}), 0);
	assert_eq!(World::from("0_").chi(RealPos{row:0,col:0}), 2);
	assert_eq!(World::from(
		"\
0
_").chi(RealPos{row:0,col:0}), 2);
	assert_eq!(World::from("\
___
_0_
___").chi(RealPos{row:1,col:1}), 4);
}

//#[test]
fn count_chi_test() {
	let w = World::from (
		"\
_________
_________
__4444___
__4122___
__4_03___
__4433___
_________
_________
_________"
	);
	assert_eq!(w.chi(RealPos{row:4,col:4}), 1);
	assert_eq!(w.chi(RealPos{row:3,col:3}), 1);
	assert_eq!(w.chi(RealPos{row:3,col:4}), 1);
	assert_eq!(w.chi(RealPos{row:4,col:5}), 4);
	assert_eq!(w.chi(RealPos{row:2,col:2}), 12);
}


#[test]
fn aliving_test() {
	assert_eq!(
		World::from (
			"0"
		).is_aliving(RealPos{row:0,col:0}).0,
		false
	);
	assert_eq!(
		World::from (
			"0_"
		).is_aliving(RealPos{row:0,col:0}).0,
		true
	);

	let w = World::from (
"\
877777777
876666667
875344467
875311467
8753_2467
875322467
875444467
876666667
7777_7777"
		);
	assert_eq!(w.is_aliving(RealPos{row:0,col:0}).0,false);
	assert_eq!(w.is_aliving(RealPos{row:1,col:1}).0,true);
	assert_eq!(w.is_aliving(RealPos{row:2,col:2}).0,false);
	assert_eq!(w.is_aliving(RealPos{row:3,col:3}).0,true);
}


#[test]
fn eat_test() {
	let mut w = World::from (
"\
11222
11222
11_33
44433
44433"
		);
	assert_eq!(
		w.go('0',0,0).board,
		World::from(
"\
_____
_____
__0__
_____
_____"
		).board
	);
	let mut w = World::from (
"\
11222
11222
11_33
44433
44433"
		);
	assert_eq!(
		w.go('4',0,0).board,
		World::from(
"\
_____
_____
__4__
444__
444__"
		).board
	);

	let mut w = World::from (
"\
11222
11222
11_11
33311
33311"
		);
	assert_eq!(
		w.go('1',0,0).board,
		World::from(
"\
11___
11___
11111
___11
___11"
		).board
	);

	let mut w = World::from (
"\
_1222
11222
11_11
33311
3331_"
		);
	assert_eq!(
		w.go('0',0,0).board,
		World::from(
"\
_1222
11222
11_11
33311
3331_"
		).board
	);
}
